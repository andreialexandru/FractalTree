#include <SFML/Graphics.hpp>

#include <cmath>
#include <limits>
#include <memory>


using Point2f = sf::Vector2f;

struct Line
{
    Point2f const A;
    Point2f const B;

    // Only allow move semantics
    Line() = delete;
    Line(Line const &) = delete;
    Line & operator=(Line const &) = delete;

    Line(Line && other) = default;
    Line & operator=(Line && other) = default;
};

using TriangleOffset = sf::Vector2f;

struct TreeShape
{
    // The members are not const so they can be moved.
    sf::ConvexShape polygon;

    Line l_base;
    Line r_base;
    TriangleOffset t_offset;

    // Only allow move semantics
    TreeShape() = delete;
    TreeShape(TreeShape const &) = delete;
    TreeShape & operator=(TreeShape const &) = delete;

    TreeShape(TreeShape && other) = default;
    TreeShape & operator=(TreeShape && other) = default;
};

struct TreeNode
{
    std::unique_ptr<TreeNode const> const left;
    std::unique_ptr<TreeNode const> const right;

    TreeShape const shape;

    TreeNode(std::unique_ptr<TreeNode const> l, std::unique_ptr<TreeNode const> r, TreeShape && s)
    : left(std::move(l)), right(std::move(r)), shape(std::move(s))
    {}
};


template <typename VEC_TYPE>
auto perpendicular_cw(sf::Vector2<VEC_TYPE> const & v) -> sf::Vector2<VEC_TYPE>
{
    return sf::Vector2<VEC_TYPE>(-v.y, v.x);
}

template <typename VEC_TYPE>
auto perpendicular_ccw(sf::Vector2<VEC_TYPE> const & v) -> sf::Vector2<VEC_TYPE>
{
    return sf::Vector2<VEC_TYPE>(v.y, -v.x);
}

template <typename VEC_TYPE>
auto magnitude(sf::Vector2<VEC_TYPE> const & v) -> VEC_TYPE
{
    return std::sqrt(v.x*v.x + v.y*v.y);
}

template <typename VEC_TYPE>
auto normalize(sf::Vector2<VEC_TYPE> const & v) -> sf::Vector2<VEC_TYPE>
{
    VEC_TYPE const m = magnitude(v);
    return sf::Vector2<VEC_TYPE>(v.x/m, v.y/m);
}

void print_v2f(sf::Vector2f const & v)
{
    printf("(%f,%f)\n", v.x, v.y);
}


auto generate_polygon(Line const & base, TriangleOffset offset) -> TreeShape
{
    sf::ConvexShape poly(5);
    poly.setFillColor(sf::Color::Black);
    poly.setOutlineColor(sf::Color::Green);
    poly.setOutlineThickness(1);

    // Bottom Left and Right
    poly.setPoint(0, base.A);
    poly.setPoint(1, base.B);

    auto base_vec = base.B - base.A;
    auto perpendicular = perpendicular_ccw(base_vec);

    // Top Right
    auto top_right = base.B + perpendicular;
    poly.setPoint(2, top_right);

    // Triangle Tip
    auto xoffset = base.A + base_vec * offset.x;
    auto yoffset = perpendicular + perpendicular * offset.y;
    auto triangle_tip = xoffset + yoffset;
    poly.setPoint(3, triangle_tip);

    // Top Left
    auto top_left = base.A + perpendicular;
    poly.setPoint(4, top_left);

    return {std::move(poly), {triangle_tip, top_right}, {top_left, triangle_tip}, std::move(offset)};
}

auto generate_nodes(TreeShape && root_shape, unsigned int const level = 0) -> std::unique_ptr<TreeNode const>
{
    if (level == 0)
        return nullptr;

    auto&& l = generate_polygon(root_shape.l_base, root_shape.t_offset);
    auto&& r = generate_polygon(root_shape.r_base, root_shape.t_offset);

    return std::make_unique <TreeNode const>
         ( generate_nodes(std::move(l), level-1)
         , generate_nodes(std::move(r), level-1)
         , std::move(root_shape)
         );
}

template <typename CB>
auto for_each_node(std::unique_ptr<TreeNode const> const & root, CB cb)
{
    if (root == nullptr)
    {
        return;
    }

    cb(*root);

    for_each_node(root->left, cb);
    for_each_node(root->right, cb);
}

int main()
{
    sf::RenderWindow window(sf::VideoMode(400, 400), "Tree Fractals");

    // config
    static Line const starting_base{ {200, 800}, {300, 800} };
    static TriangleOffset const starting_tt{ 0.3, 0.25};
    static unsigned int const max_levels = 10;

    auto tree = generate_nodes({ generate_polygon(starting_base, std::move(starting_tt)) }, max_levels);

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                window.close();
            }
        }

        window.clear();

        for_each_node(tree, [&window](TreeNode const & node)
                            {
                                window.draw(node.shape.polygon);
                            });

        window.display();
    }

    return 0;
}